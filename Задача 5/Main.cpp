#include <iostream>
#include <random>
#include <algorithm> 
#include <vector>
#include "Classes.h"
using namespace std;
struct myclass {
	void operator() (Newvector* i) { cout << '\n' << (*i) ; }
} myobject;
struct mycl1 {
	bool operator()( Newvector* i, Newvector* g) {
		if ((*i) * (*i) < (*g) * (*g) ) return true;
		else return false;
	}
}mysort1;
struct mycl2 {
	bool operator()(Newvector* i, Newvector* g) {
		if ((*i) * (*i) > (*g) * (*g)) return true;
		else return false;
	}
}mysort2;
int main()
{
	cout << "Hello, Dear User!" << "\n";
	int N, min, max;
	cout << "N= " << endl;
	cin >> N;
	cout << "max =" << endl;
	cin >> max;
	cout << "min = " << endl;
	cin >> min;
	bool d;
	vector<Newvector*> myvector(N);
	for (auto i = 0; i < N; i++) {
		if (i % 2 != 0) { 
			vector1* a = new vector1();
			(*a).gen(min, max);
			myvector[i] = a;
		}
		else {
			vector2* b = new vector2();
			(*b).gen(min, max);
			myvector[i] = b;
		}
	}
	cout << "Top (0) or done (1)";
	cin >> d;
	cout << "myvector contains (without sorting):";
	for_each(myvector.begin(), myvector.end(), myobject);
	cout << "\n";
	if (d == 1) sort(myvector.begin(), myvector.end(), mysort1);
	if (d == 0) sort(myvector.begin(), myvector.end(), mysort2);
	cout << "myvector contains:";
	for_each(myvector.begin(), myvector.end(), myobject);
	cout << "\n";
	system("pause");
}