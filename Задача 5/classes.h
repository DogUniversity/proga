#include <iostream>
#include <random>
#include <algorithm> 
#include <vector>
using namespace std;
class Newvector {
private:
	vector<int> testvector{ 0, 0, 0 };
public:
	int getx() const { return testvector[0]; }
	int gety() const { return testvector[1]; }
	int getz() const { return testvector[2]; }
	void setx(int x0) { testvector[0] = x0; }
	void sety(int y0) { testvector[1] = y0; }
	void setz(int z0) { testvector[2] = z0; }
	Newvector() {
		testvector[0] = 0;
		testvector[1] = 0;
		testvector[2] = 0;
	}
	Newvector(int x0, int y0, int z0) {
		testvector[0] = x0;
		testvector[1] = y0;
		testvector[2] = z0;
	}
	void gen(int min, int max) {
		static default_random_engine generator;
		static uniform_int_distribution<int> distribution(min, max);
		generate(testvector.begin(), testvector.end(), []() {return distribution(generator);	});
		return;
	}
	virtual ostream& print(ostream& out) const { return out << "(" << getx() << "," << gety() << "," << getz() << ")"; }
	friend ostream& operator<<(ostream& out, const Newvector& v) {
		return v.print(out); 
	}
	double operator()() { return sqrt(testvector[0] * testvector[0] + testvector[1] * testvector[1] + testvector[2] * testvector[2]); }
	virtual double operator*(const Newvector& a) = 0;
};

class vector1 : public Newvector {
public:
	double operator*(const Newvector& a) { return getx() * a.getx() + gety() * a.gety() + getz() * a.getz(); }
	ostream& print(ostream& out) const { return out << "[" << getx() << "," << gety() << "," << getz() << "]"; }
};

class vector2 : public Newvector {
public:
	double operator*(const Newvector& a) { return pow( pow(getx(), -1) * pow(a.getx(), -1) + pow(gety(), -1) * pow(a.gety(), -1) + pow(getz(), -1) * pow(a.getz(), -1), -1); }
	ostream& print(ostream& out) const { return out << "<" << getx() << "," << gety() << "," << getz() << ">"; }
};