from sympy import summation, oo, Symbol
print( summation( (-1) ** Symbol('n', integer=True)  / (2 * Symbol('n', integer=True) + 1) , ( Symbol('n', integer=True) , 0, oo)).evalf(50) * 4)
print('3.1415926535897932384626433832795028841971')
