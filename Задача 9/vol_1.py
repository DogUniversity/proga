import numpy as np
from scipy import fftpack
from matplotlib import pyplot as plt
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
np.random.seed(1234)
fig, [[ax1, ax2], [ax3, ax4]] = plt.subplots(2, 2, figsize=(6, 8))
fig.text(0.5, 0.975, 'Signal filtering',
         horizontalalignment='center',
         verticalalignment='top')
a0 = 1
b0 = 0
c0 = 0
time_step = 0.02
period = 5.
time_vec = np.arange(0, 20, time_step)
oldsig = np.cos(a0 * time_vec) + np.cos(b0 * time_vec) + np.cos(c0 * time_vec)
sig = (oldsig + 0.5 * np.random.randn(time_vec.size))
sig_fft = fftpack.fft(sig)
power = np.abs(sig_fft)
sample_freq = fftpack.fftfreq(sig.size, d=time_step)
pos_mask = np.where(sample_freq > 0)
freqs = sample_freq[pos_mask]
peak_freq = freqs[power[pos_mask].argmax()]
np.allclose(peak_freq, 1./period)
high_freq_fft = sig_fft.copy()
high_freq_fft[np.abs(sample_freq) > peak_freq] = 0
filtered_sig = fftpack.ifft(high_freq_fft) 
newpower = np.abs(high_freq_fft)
newsample_freq = fftpack.fftfreq(filtered_sig.size, d=time_step,)
a1,=ax1.plot(time_vec, sig, label='Original signal')
a2,=ax2.plot(sample_freq, power)
a3,=ax3.plot(time_vec, sig, label='Original signal')
a4,=ax3.plot(time_vec, filtered_sig, linewidth=3, label='Filtered signal')
a5,=ax3.plot(time_vec, oldsig, label='Oldsig')
ax3.legend(loc='best')
a6,=ax4.plot(newsample_freq, newpower)
axcolor = 'lightgoldenrodyellow'
axomega1 = plt.axes([0.25, 0.05, 0.65, 0.03], facecolor=axcolor)
axomega2 = plt.axes([0.25, 0.08, 0.65, 0.03], facecolor=axcolor)
axomega3 = plt.axes([0.25, 0.11, 0.65, 0.03], facecolor=axcolor)
somega1 = Slider(axomega1, 'Omega1', 0, 10.0, valinit=a0)
somega2 = Slider(axomega2, 'Omega2', 0, 10.0, valinit=b0)
somega3 = Slider(axomega3, 'Omega3', 0, 10.0, valinit=c0)
def update(val):
    omega1 = somega1.val
    omega2 = somega2.val
    omega3 = somega3.val
    oldsig = np.cos(omega1 * time_vec) + np.cos(omega2 * time_vec) + np.cos(omega3 * time_vec)
    sig = (oldsig + 0.5 * np.random.randn(time_vec.size))
    sig_fft = fftpack.fft(sig)
    power = np.abs(sig_fft)
    sample_freq = fftpack.fftfreq(sig.size, d=time_step)
    pos_mask = np.where(sample_freq > 0)
    freqs = sample_freq[pos_mask] 
    peak_freq = freqs[power[pos_mask].argmax()] + omega1*0.15 + omega2*0.15 + omega3*0.15
    high_freq_fft = sig_fft.copy()
    high_freq_fft[np.abs(sample_freq) > peak_freq] = 0
    filtered_sig = fftpack.ifft(high_freq_fft)
    newpower = np.abs(high_freq_fft)
    newsample_freq = fftpack.fftfreq(filtered_sig.size, d=time_step)
    a1.set_ydata(sig)
    a2.set_ydata(sample_freq)
    a2.set_ydata(power)
    a3.set_ydata(sig)
    a4.set_ydata(filtered_sig)
    a5.set_ydata(oldsig)
    a6.set_ydata(newsample_freq)
    a6.set_ydata(newpower)
    fig.canvas.draw_idle()
somega1.on_changed(update)
somega2.on_changed(update)
somega3.on_changed(update)

resetax = plt.axes([0.8, 0.01, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
def reset(event):
    somega1.reset()
    somega2.reset()
    somega3.reset()
button.on_clicked(reset)
plt.subplots_adjust(bottom=0.2)
plt.show()