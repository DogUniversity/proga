import random
Na = int(input("Размер массива a: "))
Nb = int(input("Размер массива b: "))
a = []
b = []
i = 0
while i < Na:
    a.append(random.randint(0, 9))
    i += 1
i = 0    
while i < Nb:
    b.append(random.randint(0, 9))
    i += 1
def sequential_slice(iterable, length):
    pool = tuple(iterable)
    assert 0 < length <= len(pool)
    tails = (pool[s:] for s in range(length))
    return zip(*tails)

def sequence_in_list(sequence, lst):
    pool = tuple(sequence)
    return any((pool == s for s in sequential_slice(lst, len(pool))))

def lcs(a, b):
    if len(a) > len(b):
        a, b = b, a
    for l in reversed(range(1, len(a)+1)):
        seq = [subseq for subseq in sequential_slice(a, l) if sequence_in_list(subseq, b)]
        if seq:
            break
    return seq
print(a)
print(b)
print(lcs(a,b))
