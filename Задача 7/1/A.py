from runstats import Statistics, Regression
count = int(input("Размер массива: "))
arr = []
i = count
while i > 0:
    arr.append(int(input()))
    i -= 1
stats = Statistics(arr)
arr.sort()
print('Sr:', stats.mean())
print('Maximum:', max(arr))
print('Minimum:', min(arr))
print('Dis:', stats.variance())
print(arr)

